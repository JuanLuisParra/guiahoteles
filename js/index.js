$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:2000});
    $('#contacto').on('show.bs.modal', function(e){
        console.log('mostrando modal');
      $('#contactoBtn1').prop('disabled',true);
      $('#contactoBtn1').removeClass('btn-outline-success');
      $('#contactoBtn1').addClass('btn-secondary');
      $('#contactoBtn2').prop('disabled',true);
      $('#contactoBtn2').removeClass('btn-outline-success');
      $('#contactoBtn2').addClass('btn-secondary');

    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('modal ya se mostró');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('modal se ocultó');
      $('#contactoBtn1').prop('disabled',false);
      $('#contactoBtn1').removeClass('btn-secondary');
      $('#contactoBtn1').addClass('btn-outline-success');
      $('#contactoBtn2').prop('disabled',false);
      $('#contactoBtn2').removeClass('btn-secondary');
      $('#contactoBtn2').addClass('btn-outline-success');
    });
  });